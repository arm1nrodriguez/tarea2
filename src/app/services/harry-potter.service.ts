import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class HarryPotterService {

  constructor(private httpClient: HttpClient) { }

  getAllHarryPotterData(){
    return this.httpClient.get('http://hp-api.herokuapp.com/api/characters/house/gryffindor');
  }
}
