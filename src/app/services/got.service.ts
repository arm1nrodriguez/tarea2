import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class GotService {

  constructor(private httpClient: HttpClient) { }

  getAllGOT(){
    return this.httpClient.get('https://thronesapi.com/api/v2/Characters');
  }
}
