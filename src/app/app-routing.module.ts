import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./gameofthrone/gameofthrone.module').then(m => m.GameofthroneModule) },
  { path: 'harry-potter', loadChildren: () => import('./harry-potter/harry-potter.module').then(m => m.HarryPotterModule) },
  { path: '**', redirectTo: ''}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
