import { Component, OnInit } from '@angular/core';
import {HarryPotterService} from "../services/harry-potter.service";

@Component({
  selector: 'app-harry-potter',
  templateUrl: './harry-potter.component.html',
  styleUrls: ['./harry-potter.component.scss']
})
export class HarryPotterComponent implements OnInit {

  HPData: any;

  constructor(public hpService: HarryPotterService ) { }

  ngOnInit(): void {

    this.HPData = this.hpService.getAllHarryPotterData();

  }

}
