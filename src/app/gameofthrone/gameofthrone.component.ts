import { Component, OnInit } from '@angular/core';
import {GotService} from "../services/got.service";

@Component({
  selector: 'app-gameofthrone',
  templateUrl: './gameofthrone.component.html',
  styleUrls: ['./gameofthrone.component.scss']
})
export class GameofthroneComponent implements OnInit {

  GOT: any;

  constructor(public gotService: GotService) { }

  ngOnInit(): void {

    this.GOT = this.gotService.getAllGOT();

  }

}
